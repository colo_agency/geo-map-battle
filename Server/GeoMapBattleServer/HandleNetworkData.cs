﻿///My version

using System;
using System.Text;
using System.Collections.Generic;
using Bindings;

namespace GeoMapBattleServer
{
    class HandleNetworkData
    {
        private delegate void Packet_(int index, byte[] data);
        private static Dictionary<int, Packet_> Packets;

        public void InitializeNetworkPackages()
        {
            Packets = new Dictionary<int, Packet_>
            {
                { (int)ClientPackets.CNewAccount, HandleNewAccount},
                { (int)ClientPackets.CLogin, HandleLogin},
                { (int)ClientPackets.CQuit, HandleQuit},
                { (int)ClientPackets.CKey, HandleKey},
                { (int)ClientPackets.СBuyMechan, HandleMechanBuy},
                { (int)ClientPackets.CFindBattle, RegisterForBattle}
            };
        }

        public void HandleNetworkInformation(int index, byte[] data)
        {
            int packetnum; PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            packetnum = buffer.ReadInteger();
            buffer.Dispose();
            if (Packets.TryGetValue(packetnum, out Packet_ Packet))
            {
                Packet.Invoke(index, data);
            }
        }

        void HandleNewAccount(int index, byte[] data)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            string decData = buffer.ReadEncryptedString();

            string username = decData.Split('/')[0];
            string password = decData.Split('/')[1];
            string email = decData.Split('/')[2];

            Core.mysql.AddAccount(index, username, password, email);

            buffer.Dispose();
        }

        private static void HandleLogin(int index, byte[] data)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            string decData1 = buffer.ReadEncryptedString();
            string decData2 = buffer.ReadEncryptedString();
            Console.WriteLine("decData1 : " + decData1); // [labu] DEbug
            Console.WriteLine("decData2 : " + decData2);     
            string username = decData1.Split('/')[0];
            string password = decData1.Split('/')[1];

            string location = decData2.Split('/')[0];
            int test = int.Parse(decData2.Split('/')[1]);
            

            string city_name = "";
            string country_name = "";
            string original_text = location;

            if (test == 1 && location != null && location != "")
            {
                try
                {
                    int first = location.IndexOf("\"id\": \"place.");
                    location = location.Substring(first);
                    first = location.IndexOf("text\":") + 8;
                    int second = location.IndexOf("\n") - 24;
                    city_name = location.Substring(first, second);

                    first = original_text.IndexOf("\"id\": \"country.");
                    original_text = original_text.Substring(first);
                    first = original_text.IndexOf("text\":") + 8;
                    second = original_text.IndexOf("\n") - 16;
                    country_name = original_text.Substring(first, second);

                    Core.mysql.CheckAccount(index, username, password, country_name, city_name);
                }
                catch
                {
                    Core.stcp.SendLogin(index, 4); //Не удалось определить местоположение игрока
                }
            }
            else
            {
                Core.mysql.CheckAccount(index, username, password, country_name, city_name);
            }

            buffer.Dispose();
        }

        private static void HandleQuit(int index, byte[] data)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            Core.stcp.SendQuit(index);

            buffer.Dispose();
        }

        private static void HandleKey(int index, byte[] data)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            int blocks = buffer.ReadEncryptedInteger();
            string[] keyBlocks = new string[blocks];

            for (int i = 0; i < keyBlocks.Length; i++)
            {
                keyBlocks[i] = buffer.ReadEncryptedString();
            }

            string clientKey = String.Join("", keyBlocks);
            Core.security.ReceiveClientKey(index, clientKey);
            buffer.Dispose();
        }

        private static void HandleMechanBuy(int index, byte[] data)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            string decData1 = buffer.ReadEncryptedString();
            Console.WriteLine("decData1 : " + decData1); // [labu] DEbug
            int mechType = Int32.Parse(decData1);
            Hangar.BMShop.BuyMechan(index, mechType);      
            buffer.Dispose();
        }

        private static void RegisterForBattle(int index, byte[] data)
        {
            //TODO add list of registred players. CHoose between them
        }
    }
}
