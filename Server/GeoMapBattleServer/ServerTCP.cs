﻿using System;
using System.Net.Sockets;
using System.Net;
using Bindings;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GeoMapBattleServer
{
    class ServerTCP
    {
        private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);       

        private static byte[] _buffer = new byte[1024];
        public static Client[] _clients = new Client[Constants.MAX_PLAYERS + 1];

        public bool IsPlayerActive(int index)
        {
            if (_clients[index] != null)
            {
                if(Core.player[index].ingame)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public byte[] PlayerData(int player, int clientKey)
        {
            PacketBuffer buffer = new PacketBuffer();

            if (player > Constants.MAX_PLAYERS + 1)
            {
                return null;
            }

            buffer.WriteInteger((int)ServerPackets.SIngame);

            string encryptionKey = Core.security.clientKey[clientKey];

            int id = Core.player[player].id;
            float posX = Core.player[player].posX;
            float posY = Core.player[player].posY;
            float posZ = Core.player[player].posZ;
            
            string username = Core.player[player].username;
            //[labu]
            Core.mysql.LoadPlayer(player, username);
            // TODO move login, crystall, rank to HandleLogin
            int crystal = Core.player[player].crystal;
            int rank = Core.player[player].rank;
            //[labu]
            string encData = id + "/" + posX + "/" + posY + "/" + posZ 
                + "/" + username + "/" + crystal + "/" + rank;

            Console.WriteLine("Передаем строку + " + encData);
            buffer.WriteEncryptedString(encryptionKey, encData);
            CheckFirstLogin(player);
            return buffer.ToArray();
        }

        void CheckFirstLogin(int player)
        {
            int crystal = Core.player[player].crystal;
            int rank = Core.player[player].rank;
            if(rank == 0)
            {
                Core.player[player].rank = 1;
                Core.player[player].crystal = 1000;
                Core.mysql.SavePlayer(player);
            }
        }
        public void SetupServer()
        {
            for (int i = 1; i < Constants.MAX_PLAYERS + 1; i++)
            {
                _clients[i] = new Client();
                Core.player[i] = new Player();
            }
            _serverSocket.Bind(new IPEndPoint(IPAddress.Any, 25565));
            _serverSocket.Listen(10);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            Socket socket = _serverSocket.EndAccept(ar);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

            for (int i = 1; i < Constants.MAX_PLAYERS + 1; i++)
            {
                if (_clients[i].socket == null)
                {
                    _clients[i].socket = socket;
                    _clients[i].index = i;
                    _clients[i].ip = socket.RemoteEndPoint.ToString();
                    _clients[i].StartClient();

                    //RSA
                    Core.stcp.SendKey(i);

                    Console.WriteLine("Установлено соединение с '{0}'", _clients[i].ip);
                    return;
                }
            }
        }

        public void SendDataTo(int index, byte[] data)
        {
            byte[] sizeinfo = new byte[8];
            sizeinfo[0] = (byte)data.Length;
            sizeinfo[1] = (byte)(data.Length >> 16);
            sizeinfo[2] = (byte)(data.Length >> 24);
            sizeinfo[3] = (byte)(data.Length >> 32);

            try
            {
                _clients[index].socket.Send(sizeinfo);
                _clients[index].socket.Send(data);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void SendDataByRegion(int index, byte[] data)
        {
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Core.player[i].ingame == true && Core.player[i].country.Equals(Core.player[index].country) && Core.player[i].region.Equals(Core.player[index].region))
                {
                    SendDataTo(i, data);
                }
            }
        }

        public void SendDataToAll(byte[] data)
        {
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Core.player[i].ingame == true)
                {
                    SendDataTo(i, data);
                }
            }
        }

        public void SendLogin(int id, int login)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteInteger((int)ServerPackets.SLogin);
            int maxPlayers = Constants.MAX_PLAYERS;
            string encryptionKey = Core.security.clientKey[id];

            MySQL.SqlBMLoad.LoadPlayersBattleMechs(id);
            

            string decData = login + "/" + id + "/" + maxPlayers;

            buffer.WriteEncryptedString(encryptionKey ,decData);
            SendDataTo(id, buffer.ToArray());
            buffer.Dispose();
            foreach (BattleMechan mechan in Core.player[id].battleMechans)
            {
                SendSavedMechan(mechan, id);
            }
        }

        public void SendSavedMechan(BattleMechan mechan, int id)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteInteger((int)ServerPackets.SMechanInfoRead);
            string encryptionKey = Core.security.clientKey[id];

            string decData = mechan.TypeId + "/" +
                mechan.Mortar + "/" +
                mechan.Main + "/" +
                mechan.Air + "/" +
                mechan.Drones + "/" +
                mechan.Radar;

            buffer.WriteEncryptedString(encryptionKey, decData);
            SendDataTo(id, buffer.ToArray());
            buffer.Dispose();
        }
        public void SendNewAccount(int index, int registration)
        {          
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteInteger((int)ServerPackets.SNewAccount);
            string key = Core.security.clientKey[index];
            string encData = registration + "/" + index;
            buffer.WriteEncryptedString(key, encData);
            SendDataTo(index, buffer.ToArray());
            buffer.Dispose();
        }

        public async void SendIngame(int index)
        {
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Core.player[i].ingame == true && Core.player[i].country.Equals(Core.player[index].country) && Core.player[i].region.Equals(Core.player[index].region))
                {
                    SendDataTo(i, PlayerData(index, i));
                }
            }

            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (IsPlayerActive(i) && Core.player[i].country.Equals(Core.player[index].country) && Core.player[i].region.Equals(Core.player[index].region))
                {
                    if (i != index)
                    {
                        await Task.Delay(75);
                        SendDataTo(index, PlayerData(i, index));
                    }
                }
            }
        }

        public void SendQuit(int index)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteInteger((int)ServerPackets.SQuit);
            buffer.WriteInteger(index);

            //Отправка всем игрока АФК
            SendDataByRegion(index, buffer.ToArray());

            buffer.Dispose();
        }

        public void SendKey(int index)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteInteger((int)ServerPackets.SKey);
            string key = Core.security.ServerKey();
            buffer.WriteString(key);
            SendDataTo(index, buffer.ToArray());
            buffer.Dispose();
        }
    }

    class Client
    {
        public int index;
        public string ip;
        public Socket socket;
        public bool closing = false;
        private byte[] _buffer = new byte[1024];

        public void StartClient()
        {
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            closing = false;
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;

            try
            {
                int received = socket.EndReceive(ar);
                if (received <= 0)
                {
                    CloseClient(index);
                }
                else
                {
                    byte[] dataBuffer = new byte[received];
                    Array.Copy(_buffer, dataBuffer, received);
                    Core.shnd.HandleNetworkInformation(index, dataBuffer);
                    socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                CloseClient(index);
                //Console.WriteLine(ex);
            }
        }

        public void OnRegistrationEnd(int index)
        {
            CloseClient(index);
        }

        public void CloseClient(int index)
        {
            Core.stcp.SendQuit(index);
            Core.player[index].ingame = false;
            Core.instance.OnlinePlayers--;
            Core.mysql.SavePlayer(index);
            closing = true;

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("{0} отключился.", Core.player[index].username);
            Console.ResetColor();

            socket.Close();
            socket = null;
        }
    }
}
