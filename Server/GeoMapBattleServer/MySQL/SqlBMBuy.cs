﻿using System;
using MySql.Data.MySqlClient;
using Bindings;

namespace GeoMapBattleServer.MySQL
{
    static class SqlBMBuy
    {
        
        //добавляем новую запись о механе типа mechanid, который пренадлежит игроку id 
        public static void AddPlayerBattleMechans(int id, int mechanid)
        {
            BattleMechan mechan = Core.player[id].GetMechanById(mechanid);

            string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();

            //команда sql
            string sql = "INSERT INTO battle_mechans" +
                " (users_idusers, mechan_id) VALUES (" +
                "'" + id + "','" + mechanid + "')";              
            MySqlCommand command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
            conn.Close();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Сохраняем БМ в базу : " + sql);
            Console.ResetColor();
        }
    }
}
