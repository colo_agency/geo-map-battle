﻿using System;
using MySql.Data.MySqlClient;
using Bindings;
using GeoMapBattleServer.MySQL;

namespace GeoMapBattleServer
{
    class MySql
    {
        public void InitializeMySQL()
        {
            string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                conn.Open();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Подключение к БД");
                Console.ResetColor();
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Подключение к БД");
                Console.ResetColor();
            }
        }

        public void CheckAccount(int index, string username, string password, string country, string region)
        {
            if (LoginExists(username)) //Проверяем существует ли логин
            {
                if (!Ingame(username))
                {
                    string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

                    MySqlConnection conn = new MySqlConnection(connStr);

                    conn.Open();
                    string sql = "SELECT password FROM users WHERE login='" + username + "'";
                    MySqlCommand command = new MySqlCommand(sql, conn);
                    string val = command.ExecuteScalar().ToString();
                    if (val.Equals(password)) //Сверяем пароль
                    {
                        Core.instance.OnlinePlayers++;
                        //Core.mysql.LoadPlayer(index, username);

                        Core.player[index].ingame = true;
                        Core.player[index].id = index;
                        Core.player[index].username = username;
                        Core.player[index].country = country;
                        Core.player[index].region = region;

                        Core.stcp.SendLogin(index, 0); //Отправка результата игроку
                        Core.stcp.SendIngame(index);

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine(username + " присоеденился к игре.");
                        Console.ResetColor();


                    }
                    else
                    {
                        Core.stcp.SendLogin(index, 1); //Не верный пароль
                    }

                    conn.Close();
                }
                else
                {
                    Core.stcp.SendLogin(index, 2); //Уже в игре
                }
            }
            else
            {
                Core.stcp.SendLogin(index, 3); //Не верный логин
            }
        }

        public bool Ingame(string login)
        {
            bool ingame = false;
            for (int i = 1; i < Constants.MAX_PLAYERS + 1; i++)
            {
                if (Core.player[i].username != null && Core.player[i].username.Equals(login))
                {
                    ingame = Core.player[i].ingame;
                }
            }

            return ingame;
        }

        public bool LoginExists(string login)
        {
            if (login == "" || login == null)
            {
                return false;
            }
            else
            {
                string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

                MySqlConnection conn = new MySqlConnection(connStr);

                conn.Open();
                string sql = "SELECT COUNT(*) FROM users WHERE login=@userLogin";
                MySqlCommand command = new MySqlCommand(sql, conn);
                command.Parameters.AddWithValue("userLogin", login);
                int val = Convert.ToInt32(command.ExecuteScalar());

                if (val > 0)
                {
                    conn.Close();
                    return true;
                }
                else
                {
                    conn.Close();
                    return false;
                }
            }
        }

        public bool EmailExists(string email)
        {
            if (email == "" || email == null)
            {
                return false;
            }
            else
            {
                string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

                MySqlConnection conn = new MySqlConnection(connStr);

                conn.Open();
                string sql = "SELECT COUNT(*) FROM users WHERE email=@userEmail";
                MySqlCommand command = new MySqlCommand(sql, conn);
                command.Parameters.AddWithValue("userEmail", email);
                int val = Convert.ToInt32(command.ExecuteScalar());

                if (val > 0)
                {
                    conn.Close();
                    return true;
                }
                else
                {
                    conn.Close();
                    return false;
                }
            }
        }

        public void AddAccount(int index, string username, string password, string email)
        {
            if (!LoginExists(username)) //Проверяем существует ли логин
            {
                if (username != "" && password != "" && email != "") //Проверяем поля
                {
                    if (!EmailExists(email))
                    {
                        string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

                        MySqlConnection conn = new MySqlConnection(connStr);

                        conn.Open();

                        string sql = "INSERT INTO users (login, password, email) VALUES ('" + username + "'," + "'" + password + "','" + email + "')";
                        MySqlCommand command = new MySqlCommand(sql, conn);
                        command.ExecuteNonQuery();
                        conn.Close();

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Добавлен новый пользователь: " + username);
                        Console.ResetColor();

                        Core.stcp.SendNewAccount(index, 0); //Регистрация
                    }
                    else
                    {
                        Core.stcp.SendNewAccount(index, 1); //Email уже используется
                    }
                }
                else
                {
                    Core.stcp.SendNewAccount(index, 2); //Нужно заполнить все поля
                }
            }
            else
            {
                Core.stcp.SendNewAccount(index, 3); //Логин уже используется
            }
        }

        public void LoadPlayer(int index, string username)
        {
            string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();

            string sql = "SELECT * FROM users WHERE login='" + username + "'";

            MySqlCommand command = new MySqlCommand(sql, conn);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                //Количество в зав. от запрашиваемых столбцов таблицы 
                //TODO что это такое? мы формируем строку, чтобы рассформировать? надо сделать нормально
                string val = (reader[4].ToString() + "/" + reader[5].ToString());
                string[] playerData = val.Split('/');
                Int32.TryParse(playerData[0], out Core.player[index].crystal);
                Int32.TryParse(playerData[1], out Core.player[index].rank);
            }

          
            reader.Close();
            conn.Close();
        }

        public void SavePlayer(int index)
        {
            string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();

            //string sql = "n";
            string sql = "UPDATE users SET crystal='" + Core.player[index].crystal + "',rank='" + Core.player[index].rank +"'";

            MySqlCommand command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();

            conn.Close();
        }


    }
}
