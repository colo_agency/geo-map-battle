﻿using MySql.Data.MySqlClient;
using System;


namespace GeoMapBattleServer.MySQL
{
    static class SqlBMLoad
    {
        public static void LoadPlayersBattleMechs(int index)
        {
            string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();

            string sql = "SELECT * " +
                "FROM battle_mechans " +
                "WHERE users_idusers='" + index + "'";

            MySqlCommand command = new MySqlCommand(sql, conn);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                int mechId = reader.GetInt32(1);
                Core.player[index].AddBattleMechan(mechId);
                BattleMechan mechan = Core.player[index].GetMechanById(mechId);
                mechan.Mortar = reader.GetInt32(2);
                mechan.Main = reader.GetInt32(3);
                mechan.Air = reader.GetInt32(4);
                mechan.Drones = reader.GetInt32(5);
                mechan.Radar = reader.GetInt32(6);
            }


            reader.Close();
            conn.Close();
        }
    }
}
