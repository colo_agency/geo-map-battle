﻿using MySql.Data.MySqlClient;
using System;

namespace GeoMapBattleServer.MySQL
{
    static class SqlBMSave
    {
        //сохраняем данные о механе типа mechanid, который пренадлежит игроку id
        public static void SavePlayerBattleMechans(int id, int mechanid)
    {
        BattleMechan mechan = Core.player[id].GetMechanById(mechanid);

        string connStr = "server=localhost;user=root;database=geomapbattle;password=;SslMode=None;";

        MySqlConnection conn = new MySqlConnection(connStr);

        conn.Open();

        //команда sql
        string sql = "UPDATE battle_mechans" +
            " SET mechan_id='" + mechanid +
            "',mortar='" + mechan.Mortar +
            "',main='" + mechan.Main +
            "',air='" + mechan.Air +
            "',drones='" + mechan.Drones +
            "',radar='" + mechan.Radar +
            "' WHERE users_idusers='" + id + "'";
        MySqlCommand command = new MySqlCommand(sql, conn);
        command.ExecuteNonQuery();
        conn.Close();

        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine("Сохраняем БМ в базу : " + sql);
        Console.ResetColor();
    }
}
}
