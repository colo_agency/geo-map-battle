﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoMapBattleServer
{
    class BattleMechan
    {
        int type = 0;
        String name;
        int main = 0;
        int mortar = 0;
        int air = 0;
        int drones = 0;
        int radar = 0;

        int mainPremium = 0;
        int mortarPremium = 0;
        int airPremium = 0;
        int dronesPremium = 0;
        int radarPremium = 0;

        public string Name { get => name; set => name = value; }
        public int Main { get => main; set => main = value; }
        public int Mortar { get => mortar; set => mortar = value; }
        public int Air { get => air; set => air = value; }
        public int Drones { get => drones; set => drones = value; }
        public int Radar { get => radar; set => radar = value; }
        public int MainPremium { get => mainPremium; set => mainPremium = value; }
        public int MortarPremium { get => mortarPremium; set => mortarPremium = value; }
        public int AirPremium { get => airPremium; set => airPremium = value; }
        public int DronesPremium { get => dronesPremium; set => dronesPremium = value; }
        public int RadarPremium { get => radarPremium; set => radarPremium = value; }

        public int TypeId { get => type; }
        public BattleMechan(int type)
        {
            this.type = type;
        }



    }
}
