﻿using Bindings;
using GeoMapBattleServer.MySQL;

namespace GeoMapBattleServer
{
    class Core
    {
        public static Core instance = new Core();

        //Инстансы классов
        public static Security security = new Security();
        public static ServerTCP stcp = new ServerTCP();
        public static HandleNetworkData shnd = new HandleNetworkData();
        public static MySql mysql = new MySql();

        //public static TempPlayer[] tempPlayer = new TempPlayer[Constants.MAX_PLAYERS + 1];
        public static Player[] player = new Player[Constants.MAX_PLAYERS + 1];

        public int OnlinePlayers;
    }
}
