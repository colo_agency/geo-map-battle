﻿using System;
using System.Collections.Generic;

namespace GeoMapBattleServer
{
    class Player
    {
        public int id;
        public string country;
        public string region;
        public string username;

        //[labu]
        public int crystal;
        public int rank;

        // should be initialized when first login //без понятия что я имел в виду
        public List<BattleMechan> battleMechans = new List<BattleMechan>(); 
        ///[labu]

        public float posX;
        public float posY;
        public float posZ;

        public bool ingame;

        public void AddBattleMechan(int type)
        {
            Console.WriteLine("AddBattleMechan");
            battleMechans.Add(new BattleMechan(type));
        }

        public BattleMechan GetMechanById(int mechanId)
        {
            foreach(BattleMechan mechan in battleMechans)
            {
                if(mechan.TypeId == mechanId)
                {
                    return mechan;
                }
            }
            return null;
        }
    }
}
