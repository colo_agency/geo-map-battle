﻿using System;
using Bindings;

namespace GeoMapBattleServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Core.security.InitRSA();

            Core.shnd.InitializeNetworkPackages();
            Core.mysql.InitializeMySQL();
            Core.stcp.SetupServer();

            while (true)
            {
                string command = Console.ReadLine();
                if (command == "exit")
                {
                    break;
                }
                else if (command == "test")
                {
                    Core.stcp.SendKey(1);
                }
                else if (command == "clear")
                {
                    Console.Clear();
                }
            }
        }
    }
}
