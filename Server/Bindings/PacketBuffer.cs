﻿using System;
using System.Collections.Generic;
using System.Text;
using GeoMapBattleServer;
using System.Linq;

namespace Bindings
{
    class PacketBuffer : IDisposable
    {
        List<byte> _bufferlist;
        byte[] _readbuffer;
        int _readpos;
        bool _buffupdate = false;

        public PacketBuffer()
        {
            _bufferlist = new List<byte>();
            _readpos = 0;
        }

        public int GetReadPos()
        {
            return _readpos;
        }

        public byte[] ToArray()
        {
            return _bufferlist.ToArray();
        }

        public int Count()
        {
            return _bufferlist.Count;
        }

        public int Length()
        {
            return Count() - _readpos;
        }

        public void Clear()
        {
            _bufferlist.Clear();
            _readpos = 0;
        }

        //Write Data
        public void WriteBytes(byte[] input)
        {
            _bufferlist.AddRange(input);
            _buffupdate = true;
        }
        public void WriteByte(byte input)
        {
            _bufferlist.Add(input);
            _buffupdate = true;
        }
        public void WriteInteger(int input)
        {
            _bufferlist.AddRange(BitConverter.GetBytes(input));
            _buffupdate = true;
        }
        public void WriteFloat(float input)
        {
            _bufferlist.AddRange(BitConverter.GetBytes(input));
            _buffupdate = true;
        }
        public void WriteString(string input)
        {
            _bufferlist.AddRange(BitConverter.GetBytes(input.Length));
            _bufferlist.AddRange(Encoding.UTF8.GetBytes(input));
            _buffupdate = true;
        }

        //Write Encrypted Data
        public void WriteEncryptedBytes(string key, byte[] input)
        {
            byte[] encData = Core.security.Encrypt(key, input);

            _bufferlist.AddRange(BitConverter.GetBytes(encData.Length));
            _bufferlist.AddRange(encData);
            _buffupdate = true;
        }
        public void WriteEncryptedInteger(string key, int input)
        {
            byte[] encData = Core.security.Encrypt(key, BitConverter.GetBytes(input));

            _bufferlist.AddRange(BitConverter.GetBytes(encData.Length));
            _bufferlist.AddRange(encData);
            _buffupdate = true;
        }
        public void WriteEncryptedFloat(string key, float input)
        {
            byte[] encData = Core.security.Encrypt(key, BitConverter.GetBytes(input));

            _bufferlist.AddRange(BitConverter.GetBytes(encData.Length));
            _bufferlist.AddRange(encData);
            _buffupdate = true;
        }
        public void WriteEncryptedString(string key, string input)
        {
            byte[] encData = Core.security.Encrypt(key, Encoding.UTF8.GetBytes(input));

            _bufferlist.AddRange(BitConverter.GetBytes(encData.Length));
            _bufferlist.AddRange(encData);
            _buffupdate = true;
        }      

        //Read Data
        public int ReadInteger(bool peek = true)
        {
            if (_bufferlist.Count > _readpos)
            {
                if (_buffupdate)
                {
                    _readbuffer = _bufferlist.ToArray();
                    _buffupdate = false;
                }

                int value = BitConverter.ToInt32(_readbuffer, _readpos);
                if (peek & _bufferlist.Count > _readpos)
                {
                    _readpos += 4;
                }
                return value;
            }
            else
            {
                throw new Exception("Буфер превысил лимит! INT");
            }
        }
        public float ReadFloat(bool peek = true)
        {
            if (_bufferlist.Count > _readpos)
            {
                if (_buffupdate)
                {
                    _readbuffer = _bufferlist.ToArray();
                    _buffupdate = false;
                }

                float value = BitConverter.ToSingle(_readbuffer, _readpos);
                if (peek & _bufferlist.Count > _readpos)
                {
                    _readpos += 4;
                }
                return value;
            }
            else
            {
                throw new Exception("Буфер превысил лимит! FLOAT");
            }
        }
        public byte ReadByte(bool peek = true)
        {
            if (_bufferlist.Count > _readpos)
            {
                if (_buffupdate)
                {
                    _readbuffer = _bufferlist.ToArray();
                    _buffupdate = false;
                }

                byte value = _readbuffer[_readpos];
                if (peek & _bufferlist.Count > _readpos)
                {
                    _readpos += 1;
                }
                return value;
            }
            else
            {
                throw new Exception("Буфер превысил лимит! BYTE");
            }
        }
        public byte[] ReadBytes(int length, bool peek = true)
        {
            if (_buffupdate)
            {
                _readbuffer = _bufferlist.ToArray();
                _buffupdate = false;
            }

            byte[] value = _bufferlist.GetRange(_readpos, length).ToArray();
            if (peek & _bufferlist.Count > _readpos)
            {
                _readpos += length;
            }
            return value;
        }
        public string ReadString(bool peek = true)
        {
            int length = ReadInteger(true);
            if (_buffupdate)
            {
                _readbuffer = _bufferlist.ToArray();
                _buffupdate = false;
            }

            string value = Encoding.UTF8.GetString(_readbuffer, _readpos, _readbuffer.Length - _readpos);
            if (peek & _bufferlist.Count > _readpos)
            {
                _readpos += length;
            }
            return value;
        }

        //Read Encrypted Data
        public int ReadEncryptedInteger(bool peek = true)
        {
            int length = ReadInteger(true);
            if (_buffupdate)
            {
                _readbuffer = _bufferlist.ToArray();
                _buffupdate = false;
            }

            byte[] value = _bufferlist.GetRange(_readpos, length).ToArray();
            if (peek & _bufferlist.Count > _readpos)
            {
                _readpos += length;
            }

            byte[] decData = Core.security.Decrypt(Core.security.KeyPath(Core.security.privateKeyPath), value);

            return BitConverter.ToInt32(decData, 0);
        }
        public float ReadEncryptedFloat(bool peek = true)
        {
            int length = ReadInteger(true);
            if (_buffupdate)
            {
                _readbuffer = _bufferlist.ToArray();
                _buffupdate = false;
            }

            byte[] value = _bufferlist.GetRange(_readpos, length).ToArray();
            if (peek & _bufferlist.Count > _readpos)
            {
                _readpos += length;
            }

            byte[] decData = Core.security.Decrypt(Core.security.KeyPath(Core.security.privateKeyPath), value);

            return BitConverter.ToSingle(decData, 0);
        }
        public byte[] ReadEncryptedBytes(bool peek = true)
        {
            int length = ReadInteger(true);
            if (_buffupdate)
            {
                _readbuffer = _bufferlist.ToArray();
                _buffupdate = false;
            }

            byte[] value = _bufferlist.GetRange(_readpos, length).ToArray();
            if (peek & _bufferlist.Count > _readpos)
            {
                _readpos += length;
            }

            byte[] decData = Core.security.Decrypt(Core.security.KeyPath(Core.security.privateKeyPath), value);

            return decData;
        }
        public string ReadEncryptedString(bool peek = true)
        {
            int length = ReadInteger(true);
            if (_buffupdate)
            {
                _readbuffer = _bufferlist.ToArray();
                _buffupdate = false;
            }

            byte[] value = _bufferlist.GetRange(_readpos, length).ToArray();
            if (peek & _bufferlist.Count > _readpos)
            {
                _readpos += length;
            }

            byte[] decData = Core.security.Decrypt(Core.security.KeyPath(Core.security.privateKeyPath), value);
            Console.WriteLine("Получено: " + Encoding.UTF8.GetString(value));
            Console.WriteLine("Расшифровано: " + Encoding.UTF8.GetString(decData));
            return Encoding.UTF8.GetString(decData);
        }      

        //IDisposable
        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _bufferlist.Clear();
                }
                _readpos = 0;
            }
            disposedValue = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
