﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bindings
{
    //Отправка с сервера на клиент
    public enum ServerPackets
    {
        SNewAccount = 1,
        SLogin = 2,
        SIngame = 3,
        SQuit = 4,
        SKey = 5,
        SBuyMechan = 6,
        SMechanInfoRead = 7,
        SFindBattle = 8
    }

    //Отправка с клиента на сервер
    public enum ClientPackets
    {
        CNewAccount = 1,
        CLogin = 2,
        CQuit = 3,
        CKey = 4,
        СBuyMechan = 6,
        CFindBattle = 8,
    }
}
