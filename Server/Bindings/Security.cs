﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Bindings
{
    public class Security
    {
        public RSAParameters privateKey;
        public RSAParameters publicKey;

        public string publicKeyPath = @"Keys\publicKey.xml";
        public string privateKeyPath = @"Keys\privateKey.xml";
        public string[] clientKey = new string[Constants.MAX_PLAYERS + 1];

        public void ReceiveClientKey(int index, string key)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            clientKey[index] = key;
        }

        public string ServerKey()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            string publicKey = File.ReadAllText(publicKeyPath);
            rsa.FromXmlString(publicKey);
            return rsa.ToXmlString(false);
        }

        public void InitRSA()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.PersistKeyInCsp = false;

            if (File.Exists(privateKeyPath)) File.Delete(privateKeyPath);

            if (File.Exists(publicKeyPath)) File.Delete(publicKeyPath);

            string publicKey = rsa.ToXmlString(false);
            File.WriteAllText(publicKeyPath, publicKey);
            string privateKey = rsa.ToXmlString(true);
            File.WriteAllText(privateKeyPath, privateKey);
        }

        public byte[] Encrypt(string EncKey, byte[] data)
        {
            byte[] encData;
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

                rsa.FromXmlString(EncKey);
                encData = rsa.Encrypt(data, true);

                return encData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public byte[] Decrypt(string DecKey, byte[] data)
        {
            byte[] decData;
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

                rsa.FromXmlString(DecKey);
                decData = rsa.Decrypt(data, true);
                return decData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public string KeyPath(string path)
        {
            return File.ReadAllText(path);
        }
    }
}
