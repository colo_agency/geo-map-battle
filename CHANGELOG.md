# **GeoMapBattle_v0.0.1.2_b1_20200126_0506PM**

---

## **Added**

---

- Localization of messages for errors of registration

## **Removed**

---

- Deleted scene from Build Settings

# **GeoMapBattle_v0.0.1.1_b1_20200126_0433PM**

----

## **Updated**

----

- Unity version to 2018.4.15f1

## **Added**

---

- Changelog
- Test apk

## **Changed**

---

- Local IP address
- Readme