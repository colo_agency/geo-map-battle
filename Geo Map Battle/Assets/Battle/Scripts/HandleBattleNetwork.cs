﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Network;

public class HandleBattleNetwork : MonoBehaviour
{
    private delegate void SendString(string s);

    private static Dictionary<int, SendString> Packets;

    public static void InitializeNetworkPackages()
    {
        Packets = new Dictionary<int, SendString>
            {
                { (int)BattlePackages.ShootMain, SendShootMain},
            };
    }

    public static void SendShootMain(string positionString)
    {
    }

    private void Awake()
    {
        InitializeNetworkPackages();
    }
}