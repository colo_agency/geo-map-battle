﻿using System;
using System.IO;
using UnityEngine;
using System.Security.Cryptography;

public class Security : MonoBehaviour
{
    public static Security security;

    public string privateKeyPath;
    public string publicKeyPath;
    public string serverKeyPath;

    private void Awake()
    {
        security = this;

        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        privateKeyPath = "/privateKey.xml";
        publicKeyPath = "/publicKey.xml";
        serverKeyPath = "/serverKey.xml";
        CopyToData(privateKeyPath);
        CopyToData(publicKeyPath);
        CopyToData(serverKeyPath);

        privateKeyPath = Application.persistentDataPath + privateKeyPath;
        publicKeyPath = Application.persistentDataPath + publicKeyPath;
        serverKeyPath = Application.persistentDataPath + serverKeyPath;
        Debug.Log(privateKeyPath);
    }

    private static void CopyToData(string _path)
    {
        UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest
            .Get("jar:file://" + Application.dataPath + "!/assets/StreamingAssets/" + _path);
        www.SendWebRequest();
        while (!www.isDone)
        {
        }
        File.WriteAllBytes(Application.persistentDataPath + _path, www.downloadHandler.data);
    }

    public void GenerateKeys()
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        string publicKey = rsa.ToXmlString(false);
        File.WriteAllText(publicKeyPath, publicKey);
        string privateKey = rsa.ToXmlString(true);
        File.WriteAllText(privateKeyPath, privateKey);
    }

    public string ClientKey()
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        string publicKey = File.ReadAllText(publicKeyPath);
        rsa.FromXmlString(publicKey);
        return rsa.ToXmlString(false);
    }

    public void ReceiveServerKey(string key)
    {
        GenerateKeys();
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        File.WriteAllText(serverKeyPath, key);

        ClientTCP.clientTCP.SendKey(ClientKey());
    }

    public byte[] Encrypt(string EncKey, byte[] data)
    {
        byte[] encData;
        try
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            rsa.FromXmlString(EncKey);
            encData = rsa.Encrypt(data, true);

            return encData;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }

    public byte[] Decrypt(string DecKey, byte[] data)
    {
        byte[] decData;
        try
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            rsa.FromXmlString(DecKey);
            decData = rsa.Decrypt(data, true);
            return decData;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }

    public string KeyPath(string path)
    {
        return File.ReadAllText(path);
    }
}