﻿public enum ServerPackets
{
    SNewAccount = 1,
    SLogin = 2,
    SIngame = 3,
    SQuit = 4,
    SKey = 5,
    SBuyMechan = 6,
    SMechanInfoRead = 7,
    SFindBattle = 8,
    SBeginBattle = 9,
    STransferData = 10,
    SGetPlayerPosition = 11
}

public enum ClientPackets
{
    CNewAccount = 1,
    CLogin = 2,
    CQuit = 3,
    CKey = 4,
    СBuyMechan = 6,
    CMechanInfoRead = 7,
    CFindBattle = 8,
    CBeginBattle = 9,
    CTransferData = 10,
    SGetPlayerPosition = 11,
}