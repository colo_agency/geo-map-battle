using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager networkManager;
    private static int loadMenu = 0;
    public int MyIndex;
    public int MaxPlayers;

    void Awake()
    {
        UnityThread.initUnityThread();
        if (networkManager == null)
        {
            networkManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private static NetworkManager instance;
    public static NetworkManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new NetworkManager();
            }
            return instance;
        }
    }

    //private bool Connection()
    //{
    //    if (Application.internetReachability == NetworkReachability.NotReachable)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }
    //}


    public bool Login(string login, string password, string playerCity, int locationInput)
    {

        if (!ClientTCP.clientTCP.connected)
        {
            Debug.Log("[labu] No connection");
            ClientTCP.clientTCP.InitializeConnection();
            return false;
        }
        else
        {
            ClientTCP.clientTCP.SendLogin(login, password, playerCity, locationInput);
        }
        return true;
    }
   
    public void Registration(string login, string password, string email)
    {
        
        if (!ClientTCP.clientTCP.connected)
        {
            Debug.Log("[labu] No connection -registration");
            ClientTCP.clientTCP.InitializeConnection();
        }
        else
        {
            ClientTCP.clientTCP.SendNewAccount(login, password, email);
        }
       
    }
    private void Start()
    {
        StartCoroutine(CheckForConnection());
    }
    public void Quit()
    {
        Application.Quit();
    }

    public void SavePlayerLocaly(int id, string username, float posX, float posY, float posZ, int crystal, int rank)
    { 
        Player.id = id;
        Player.username = username;
        Player.posX = posX;
        Player.posY = posY;
        Player.posZ = posZ;
        Player.crystal = crystal;
        Player.rank = rank;

    }
    public void RegisterForBattle()
    {
        ClientTCP.clientTCP.RegisterForBattle();
    }
    IEnumerator CheckForConnection()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            if (!ClientTCP.clientTCP.connected)
            {
                Debug.Log("CheckForConnection - no connection established");
                ClientTCP.clientTCP.InitializeConnection();
            }
        }
        
    }
    public void RemovePlayer(int index)
    {
        Destroy(GameObject.Find("player" + index)); // Удаление игрока оффлайн
    }

}
