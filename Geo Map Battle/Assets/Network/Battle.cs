﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
    public static class Battle
    {
        public static void ReceivePackage(byte[] data)
        {
            PacketBuffer buffer = new PacketBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            //TODO handle dictionary

            buffer.Dispose();
        }

        public static void SendString(string dataString)
        {
            PacketBuffer buffer = new PacketBuffer();

            buffer.WriteInteger((int)ClientPackets.CTransferData);
            buffer.WriteString(dataString);

            ClientTCP.clientTCP.SendData(buffer.ToArray());

            buffer.Dispose();
        }
    }
}