using System.Collections.Generic;
using UnityEngine;

public class HandleNetworkData : MonoBehaviour
{
    public static HandleNetworkData handleNetworkData;
    [SerializeField] private LoginDataReader loginDataReader;

    private delegate void Packet_(byte[] data);

    private static Dictionary<int, Packet_> Packets;
    private int message = -1;

    public int Message
    {
        get
        {
            return message;
        }

        set
        {
            message = value;
        }
    }

    public void InitializeNetworkPackages()
    {
        Packets = new Dictionary<int, Packet_>
            {
                { (int)ServerPackets.SNewAccount, HandleNewAccount },
                { (int)ServerPackets.SLogin, HandleLogin },
                { (int)ServerPackets.SIngame, HandleIngame },
                { (int)ServerPackets.SQuit, HandleQuit },
                { (int)ServerPackets.SKey, HandleKey },
                { (int)ServerPackets.SBuyMechan, BuyMechan },
                { (int)ServerPackets.SMechanInfoRead, SaveMechan },
                { (int)ServerPackets.SBeginBattle, BeginBattle },
                { (int)ServerPackets.STransferData, Network.Battle.ReceivePackage},
                { (int)ServerPackets.SGetPlayerPosition, BattleController.Instance.GetPlayerPosition}
            };
    }

    private void Awake()
    {
        handleNetworkData = this;
        InitializeNetworkPackages();
    }

    private void BuyMechan(byte[] data)
    {
        Debug.LogError("BuyMechan not Implemented");
    }

    public static void HandleNetworkInformation(byte[] data)
    {
        int packetnum;
        PacketBuffer buffer = new PacketBuffer();
        Packet_ Packet;
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        buffer.Dispose();
        if (Packets.TryGetValue(packetnum, out Packet))
        {
            Packet.Invoke(data);
        }
    }

    public void HandleLogin(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();
        string[] dData = buffer.ReadEncryptedString().Split('/');

        message = int.Parse(dData[0]);
        NetworkManager.networkManager.MyIndex = int.Parse(dData[1]);
        NetworkManager.networkManager.MaxPlayers = int.Parse(dData[2]);

        buffer.Dispose();
    }

    public void HandleNewAccount(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        string decData = buffer.ReadEncryptedString();

        int msg = int.Parse(decData.Split('/')[0]);
        int MyIndex = int.Parse(decData.Split('/')[1]);

        //string[] registrationMsg = { "Успешно зарегистрировался", "Email уже используется", "Нужно заполнить все поля", "Логин уже используется" };
        message = msg;

        NetworkManager.networkManager.MyIndex = MyIndex;

        buffer.Dispose();
    }

    public void HandleIngame(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        string decData = buffer.ReadEncryptedString();
        decData = decData.Replace(",", ".");

        int id = int.Parse(decData.Split('/')[0]);
        float posX = float.Parse(decData.Split('/')[1]);
        float posY = float.Parse(decData.Split('/')[2]);
        float posZ = float.Parse(decData.Split('/')[3]);
        string username = decData.Split('/')[4];

        int crystal = int.Parse(decData.Split('/')[5]);
        int rank = int.Parse(decData.Split('/')[6]);
        NetworkManager.networkManager.SavePlayerLocaly(id, username, posX, posY, posZ, crystal, rank);
        Debug.Log("crystal: " + crystal + " rank: " + rank);

        buffer.Dispose();
    }

    private void BeginBattle(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();
        string enemyName = buffer.ReadString();
        Debug.Log("Find oponent with name " + enemyName);
    }

    public void HandleQuit(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int id = buffer.ReadInteger();

        NetworkManager.networkManager.RemovePlayer(id);

        buffer.Dispose();
    }

    public void HandleKey(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        ClientTCP.clientTCP.connected = true;

        //NetworkManager.networkManager.ShowInfo("Установлено подключение");
        Debug.Log("Установлено подключение");
        string key = buffer.ReadString();
        Debug.Log("Received ServerKey = " + key);
        Security.security.ReceiveServerKey(key);

        buffer.Dispose();
    }

    private void SaveMechan(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();
        string[] dData = buffer.ReadEncryptedString().Split('/');
        loginDataReader.SaveBM(dData);

        buffer.Dispose();
    }

    private void HandlePositionCall(byte[] data)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        buffer.Dispose();
    }
}