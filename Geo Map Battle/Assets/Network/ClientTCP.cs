using UnityEngine;
using System.Net.Sockets;
using System;
using System.Threading;
using UnityEngine.SceneManagement;

public class ClientTCP : MonoBehaviour
{
    public static ClientTCP clientTCP;

    public string IP_ADRESS;
    public int PORT;

    [SerializeField] private MechanShop mechanShop;
    
    public Socket _clientSocket;
    public Thread SocketThread;

    private byte[] _asyncbuffer = new byte[1024];

    public bool connected;
    public bool noConnection;

    void Awake()
    {
        clientTCP = this;

        SocketThread = new Thread(OnReceive);

        SocketThread.IsBackground = true;
        SocketThread.Start();

        InitializeConnection();
    }

    public MechanShop GetMechanShop()
    {
        return mechanShop;
    }
    public void InitializeConnection()
    {
        _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        _clientSocket.BeginConnect(IP_ADRESS, PORT, new AsyncCallback(ConnectCallback), _clientSocket);
    }

    public void ConnectCallback(IAsyncResult ar)
    {
        bool result = ar.AsyncWaitHandle.WaitOne(15000, true);

        if (_clientSocket.Connected)
        {
            _clientSocket.EndConnect(ar);

            while (true)
            {
                OnReceive();
                System.Threading.Thread.Sleep(75);
            }
        }
        else
        {
            _clientSocket.Close();
        }     
    }

    public void OnReceive()
    {
        byte[] _sizeinfo = new byte[8];
        byte[] _receivedbuffer = new byte[1024];

        int totalread = 0, currentread = 0;        

        try
        {
            currentread = totalread = _clientSocket.Receive(_sizeinfo);
            if (totalread <= 0)
            {
                if (noConnection == false)
                {
                    CloseConnection();
                }

                noConnection = true;
            }
            else
            {
                while (totalread < _sizeinfo.Length && currentread > 0)
                {
                    currentread = _clientSocket.Receive(_sizeinfo, totalread, _sizeinfo.Length - totalread, SocketFlags.None);
                    totalread += currentread;

                    System.Threading.Thread.Sleep(75);
                }

                int messagesize = 0;
                messagesize |= _sizeinfo[0];
                messagesize |= (_sizeinfo[1] << 16);
                messagesize |= (_sizeinfo[2] << 24);
                messagesize |= (_sizeinfo[3] << 32);

                byte[] data = new byte[messagesize];

                totalread = 0;
                currentread = totalread = _clientSocket.Receive(data, totalread, data.Length - totalread, SocketFlags.None);
                while (totalread < messagesize && currentread > 0)
                {
                    currentread = _clientSocket.Receive(data, totalread, data.Length - totalread, SocketFlags.None);
                    totalread += currentread;

                    System.Threading.Thread.Sleep(75);
                }

                try
                {
                    UnityThread.executeInUpdate(() =>
                    {
                        HandleNetworkData.HandleNetworkInformation(data);                        
                    });
                }
                catch
                {
                    UnityThread.executeInUpdate(() =>
                    {
                        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                    });
                }

                noConnection = false;
            }
        }
        catch
        {
            if (noConnection == false)
            {
                CloseConnection();
            }

            noConnection = true;
        }      
    }

    public void OnDisable()
    {
        SocketThread.Abort();

        if (_clientSocket != null)
        {
            _clientSocket.Close();
            _clientSocket = null;
        }
    }

    public void CloseConnection()
    {
        if (_clientSocket != null)
        {
            _clientSocket.Close();
            _clientSocket = null;
        }
    }

    public void QuitClient()
    {
        SocketThread.Abort();
        _clientSocket = null;
        Application.Quit();
    }

    public void SendData(byte[] data)
    {
        try
        {
            _clientSocket.Send(data);
        }
        catch
        {
            connected = false;

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void SendNewAccount(string username, string password, string email)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteInteger((int)ClientPackets.CNewAccount);

        string encryptionKey = Security.security.KeyPath(Security.security.serverKeyPath);

        string encData = username + "/" + password + "/" + email;

        buffer.WriteEncryptedString(encryptionKey, encData);

        SendData(buffer.ToArray());
        buffer.Dispose();
    }

    public void SendLogin(string username, string password, string location, int test)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteInteger((int)ClientPackets.CLogin);

        string encryptionKey = Security.security.KeyPath(Security.security.serverKeyPath);

        string encData1 = username + "/" + password;
        string encData2 = location + "/" + test;

        buffer.WriteEncryptedString(encryptionKey, encData1);
        buffer.WriteEncryptedString(encryptionKey, encData2);

        SendData(buffer.ToArray());
        buffer.Dispose();
    }

    public void SendKey(string key)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteInteger((int)ClientPackets.CKey);        

        int pos = 0;
        int size = 80;
        int blocks = (int)Math.Ceiling(key.Length / 80f);
        string[] keyBlock = new string[blocks];

        string encryptionKey = Security.security.KeyPath(Security.security.serverKeyPath);

        buffer.WriteEncryptedInteger(encryptionKey, blocks);

        for (int i = 0; i < blocks; i++)
        {
            if (i < blocks - 1)
            {
                keyBlock[i] = key.Substring(pos, size);
                pos += size;
            }
            else
            {
                int len = key.Length - (size * (blocks - 1));              
                keyBlock[i] = key.Substring(pos, len);
                pos += size;
            }

            buffer.WriteEncryptedString(encryptionKey,keyBlock[i]);
        }

        SendData(buffer.ToArray());
        buffer.Dispose();
    }

    public void RegisterForBattle()
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteInteger((int)ClientPackets.CFindBattle);

        SendData(buffer.ToArray());
        buffer.Dispose();
    }
}
