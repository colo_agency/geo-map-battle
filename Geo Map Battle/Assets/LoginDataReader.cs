﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginDataReader : MonoBehaviour
{
    [SerializeField] UserInfo user;

    public void SaveBM(string[] data)
    {
        int mechanType = int.Parse(data[0]);
        BattleMechan mechan = user.BattleMechans.GetMechByType(mechanType);
        mechan.Mortar = int.Parse(data[1]);
        mechan.Main = int.Parse(data[2]);
        mechan.Air = int.Parse(data[3]);
        mechan.Drones = int.Parse(data[4]);
        mechan.Radar = int.Parse(data[5]);
        user.SaveBattleMechan(mechan);
    }
}
