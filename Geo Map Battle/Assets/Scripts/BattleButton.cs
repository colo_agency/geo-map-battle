﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleButton : MonoBehaviour
{
    Image buttonGreenSubstrace;
    Image buttonRedSubstrace;
    int weponIndex;
    // Start is called before the first frame update
    void Start()
    {
        buttonGreenSubstrace = gameObject.GetComponentsInChildren<Image>()[1];
        buttonRedSubstrace = gameObject.GetComponentsInChildren<Image>()[0];
        SetWeaponIndex();
        //Debug.Log(gameObject.name + buttonGreenSubstrace.gameObject.name);

        BattleController.Instance.RegisterButtleButton(this, weponIndex);
        
    }

    private void SetWeaponIndex()
    {
        switch (gameObject.name)
        {
            case "Main":
                weponIndex = 1;
                break;
            case "AntiAir":
                weponIndex = 0;
                break;
            case "Volley":
                weponIndex = 3;
                break;
            case "Burst":
                weponIndex = 2;
                break;
        }
    }

    //Method to call to set green fillin of button's substrace
    public void SetGreenFilling(float amount)
    {
        buttonGreenSubstrace.fillAmount = amount;
    }

    // Method to call when changing button's substraces
    public void ManageButtonState(int state)
    {
        //Debug.Log("managing state " + state);
        if (state == 1) // Recharging
        {
            buttonGreenSubstrace.gameObject.SetActive(true);
            buttonRedSubstrace.gameObject.SetActive(true);
        }
        else if (state == 2) // Active
        {
            buttonGreenSubstrace.gameObject.SetActive(true);
            buttonRedSubstrace.gameObject.SetActive(false);
        }
        else if (state == 0) // Inactive
        {
            buttonGreenSubstrace.gameObject.SetActive(false);
            buttonRedSubstrace.gameObject.SetActive(false);
        }
        else if (state == 3) //Pressed
        {
            buttonGreenSubstrace.gameObject.SetActive(true);
            buttonRedSubstrace.gameObject.SetActive(false);
        }

    }
}


