﻿using UnityEngine;

[CreateAssetMenu(fileName = "New User", menuName = "User")]
public class UserInfo : ScriptableObject
{
    [SerializeField]
    string login;
    [SerializeField]
    string email;
    [SerializeField]
    string password;
    [SerializeField]
    int crystal;
    [SerializeField]
    int rank;
    [SerializeField]
    BattleMechanList battleMechans;

    public string Login
    {
        get { return login; } set { login = value; }
    }
    public string Email
    {
        get { return email; } set { email = value; }
    }
    public string Password
    {
        get { return password; } set { password = value; }
    }

    public int Crystal
    {
        get { return crystal; } set { crystal = value; }
    }
    public int Rank {
        get  { return rank; } set { rank = value; }
    }
    public void SaveBattleMechan(BattleMechan bm)
    {
        BattleMechan mechan = battleMechans.GetMechByType(bm.TypeId);
        mechan.Mortar = bm.Mortar;
        mechan.Main = bm.Main;
        mechan.Air = bm.Air;
        mechan.Drones = bm.Drones;
        mechan.Radar = bm.Radar;
        mechan.IsOwned = true;
    }
    public BattleMechanList BattleMechans { get => battleMechans; }
}
