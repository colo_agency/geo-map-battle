﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBattleMechan", menuName = "Battle Mechan")]
public class BattleMechan : ScriptableObject
{
    [SerializeField]
    Sprite sprite;
    [SerializeField]
    bool isOwned;
    [SerializeField]
    int type = 0;
    [SerializeField]
    string name;
    [SerializeField]
    int main = 0;
    [SerializeField]
    int mortar = 0;
    [SerializeField]
    int air = 0;
    [SerializeField]
    int drones = 0;
    [SerializeField]
    int radar = 0;

    [SerializeField]
    int mainPremium = 0;
    [SerializeField]
    int mortarPremium = 0;
    [SerializeField]
    int airPremium = 0;
    [SerializeField]
    int dronesPremium = 0;
    [SerializeField]
    int radarPremium = 0;

    public string Name { get => name; set => name = value; }
    public int Main { get => main; set => main = value; }
    public int Mortar { get => mortar; set => mortar = value; }
    public int Air { get => air; set => air = value; }
    public int Drones { get => drones; set => drones = value; }
    public int Radar { get => radar; set => radar = value; }
    public int MainPremium { get => mainPremium; set => mainPremium = value; }
    public int MortarPremium { get => mortarPremium; set => mortarPremium = value; }
    public int AirPremium { get => airPremium; set => airPremium = value; }
    public int DronesPremium { get => dronesPremium; set => dronesPremium = value; }
    public int RadarPremium { get => radarPremium; set => radarPremium = value; }

    public int TypeId { get => type; }
    public bool IsOwned { get => isOwned; set => isOwned = value; }
    public Sprite Sprite { get => sprite; set => sprite = value; }
}
