﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BattleMechanList", menuName = "BattleMechanList")]
public class BattleMechanList : ScriptableObject
{
    [SerializeField]
    List<BattleMechan> battleMechans = new List<BattleMechan>();

    public List<BattleMechan> GetBattleMechansList()
    {
        return battleMechans;
    }

    public BattleMechan GetMechByType(int mechType)
    {
        foreach(BattleMechan mechan in battleMechans)
        {
            if(mechan.TypeId == mechType)
            {
                return mechan;
            }
        }
        return null; 
    }

}
