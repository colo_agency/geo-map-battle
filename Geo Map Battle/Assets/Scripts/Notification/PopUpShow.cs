﻿using System;
using System.Collections;
using UnityEngine;

namespace PopUp
{
    public class PopUpShow : MonoBehaviour
    {
        [SerializeField]
        protected RectTransform popupTransform;
        [SerializeField]
        protected AnimationCurve scaleCurve;
        [SerializeField]
        [Range(0.1f, 5f)]
        protected float timeForAnimation = 1f;

#if UNITY_EDITOR
        void OnValidate()
        {
            Transform popup = transform.Find("PopUp");
            if (popup)
            {
                popupTransform = popup.GetComponent<RectTransform>();
            }
            
        }
#endif

        protected IEnumerator ScaleAnimationShow()
        {
            float elapsedTime = 0f;
            while (elapsedTime < timeForAnimation)
            {
                float normalTime = elapsedTime / timeForAnimation;
                popupTransform.localScale = Vector3.one * normalTime;
                yield return null;
                elapsedTime += Time.deltaTime;
            }
            popupTransform.localScale = Vector3.one;
        }

        protected IEnumerator ScaleAnimationFade()
        {
            float elapsedTime = timeForAnimation - Time.deltaTime;
            while (elapsedTime > 0f)
            {
                float normalTime = elapsedTime / timeForAnimation;
                popupTransform.localScale = Vector3.one * normalTime;
                yield return null;
                elapsedTime -= Time.deltaTime;
            }
            popupTransform.localScale = Vector3.zero;
            gameObject.SetActive(false);
        }

        public void AnimationShow()
        {
            StartCoroutine(ScaleAnimationShow());
        }

        public void AnimationFade()
        {
            StartCoroutine(ScaleAnimationFade());
        }

    }
}
