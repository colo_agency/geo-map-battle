﻿using System;
using UnityEngine.SceneManagement;

namespace PopUp
{
    public class SurePopUp : PopUpShow, ICancelButton, IOkayButton
    {
        public void CancleButton()
        {
            AnimationFade();
        }

        public void OkayButton()
        {
            SaveManager.DeleteData();
            SceneManager.LoadScene(0);
        }
    }
}