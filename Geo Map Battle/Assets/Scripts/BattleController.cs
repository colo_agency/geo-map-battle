﻿using System.Collections;
using UnityEngine;

public class BattleController : MonoBehaviour
{
    [SerializeField] private PlayerPosition playerPosition = null;

    private int battleButtonsIndex = 0;
    private BattleButton main;
    private BattleButton volley;
    private BattleButton burst;
    private BattleButton antiAir;

    private enum States { Pressed = 3, Recharching = 1, Active = 2, Inactive = 0 };

    private States mortarState = States.Recharching;
    private States mainState = States.Recharching;
    private States antiAirState = States.Recharching;

    private float mainCurrentAmmo = 0f;
    private float mainRechargeTime = 10f;
    private float antiAirCurrentAmmo = 0f;
    private float mortarCurrentAmmo = 0f;
    private float mortarRechargeTime = 20f;
    private bool isBursting = false;
    private bool gameIsRunning = true;
    private bool firstRun = true;
    private bool antiAirShouldStop = false;
    private bool antiAirHasTarget = true;       // antiair will stop shouting, if changed to false;
    private bool mainShouldStop = false;
    private bool mortarShouldStop = false;

    private IEnumerator WeaponsStateCoroutine = null;

    // Singleton
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private static BattleController instance;

    public static BattleController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<BattleController>();
            }
            return instance;
        }
    }

    // Observer, called in BattleButtonCotroler
    public void RegisterButtleButton(BattleButton battleButton, int weaponIndex)
    {
        if (battleButtonsIndex > 3)
        {
            Debug.Log("Battle button index out of bond");
            return;
        }
        switch (weaponIndex)
        {
            case 0:
                antiAir = battleButton;
                antiAir.ManageButtonState((int)antiAirState);
                break;

            case 1:
                main = battleButton;
                main.ManageButtonState((int)mainState);
                break;

            case 2:
                burst = battleButton;
                burst.ManageButtonState((int)mortarState);
                break;

            case 3:
                volley = battleButton;
                Debug.Log(volley);
                volley.ManageButtonState((int)mortarState);
                break;
        }
        if (battleButtonsIndex == 3)
        {
            WeaponsStateCoroutine = WeaponsStateUpdater();
            StartCoroutine(WeaponsStateCoroutine);
        }
        battleButtonsIndex++;
    }

    //Main = 1, Burst = 2, Volley = 3, AntiAir = 0
    public void FireButtonPressed(int button)
    {
        if (button == 0)    //AntiAir
        {
            HandlePressedButton(ref antiAirState, antiAir, antiAirCurrentAmmo);
        }
        else if (button == 1) //Main
        {
            mortarShouldStop = true;
            HandlePressedButton(ref mainState, main, mainCurrentAmmo);
        }
        else if (button == 2) //Burst
        {
            isBursting = true;
            HandlePressedButton(ref mortarState, burst, mortarCurrentAmmo);
        }
        else if (button == 3) //Volley
        {
            HandlePressedButton(ref mortarState, volley, mortarCurrentAmmo);
        }
    }

    private void HandlePressedButton(ref States weapon, BattleButton weaponButton, float currentAmmo)
    {
        if (weapon == States.Inactive)
        {
            weapon = States.Active;
            weaponButton.ManageButtonState((int)weapon);
            if (weaponButton == burst)
            {
                mainShouldStop = true;
                volley.ManageButtonState((int)weapon);
            }
            else if (weaponButton == volley)
            {
                mainShouldStop = true;
                burst.ManageButtonState((int)weapon);
            }
            //antiAir.SetGreenFilling(currentAmmo);
        }
        else if (weapon == States.Active)
        {
            weapon = States.Pressed;
        }
    }

    //when cancle pressed
    public void StopMain()
    {
        mainShouldStop = true;
        mortarShouldStop = true;
    }

    //when StopAntiAirButton Was pressed
    public void StopAntiAir()
    {
        antiAirShouldStop = true;
        antiAirCurrentAmmo = 1f;
    }

    // coroutine working  all time game going, handle weapons states, ammo, calling fire events, etc
    private IEnumerator WeaponsStateUpdater()
    {
        while (gameIsRunning)
        {
            // handle Anti-Air
            if (antiAirState == States.Recharching)
            {
                antiAirCurrentAmmo += 1f / 0.5f * Time.deltaTime;
                antiAir.SetGreenFilling(antiAirCurrentAmmo);
                if (antiAirCurrentAmmo >= 1.0f)
                {
                    if (firstRun)
                    {
                        firstRun = false;
                        antiAirState = States.Inactive;
                    }
                    else
                    {
                        antiAirState = States.Pressed;
                    }
                    antiAir.ManageButtonState((int)antiAirState);
                }
            }
            if (antiAirShouldStop)
            {
                antiAirState = States.Inactive;
                antiAir.ManageButtonState((int)antiAirState);
                antiAirShouldStop = false;
            }
            else if (antiAirState == States.Pressed && antiAirHasTarget)
            {
                antiAirCurrentAmmo = 0f;                            //place to make AntuAir Fire() method;
                antiAir.SetGreenFilling(antiAirCurrentAmmo);
                antiAirState = States.Recharching;
                antiAir.ManageButtonState((int)antiAirState);
            }

            //handle Main calliber
            if (mainState == States.Recharching)
            {
                mainCurrentAmmo += 1f / mainRechargeTime * Time.deltaTime; // recharging
                main.SetGreenFilling(mainCurrentAmmo);
                if (mainCurrentAmmo >= 1.0f)
                {
                    mainState = States.Inactive;
                    main.ManageButtonState((int)mainState);
                }
            }
            else if (mainCurrentAmmo < 0.33f)
            {
                mainState = States.Recharching;
                main.ManageButtonState((int)mainState);
            }
            else if (mainShouldStop)
            {
                mainState = States.Inactive;
                main.ManageButtonState((int)mainState);
                mainShouldStop = false;
            }
            else if (mainState == States.Pressed)
            {
                mainCurrentAmmo -= 0.33f;                       //make main calliber Fire();
                Debug.Log(mainCurrentAmmo);
                main.SetGreenFilling(mainCurrentAmmo);
                if (mainCurrentAmmo < 0.1f)
                {
                    mainState = States.Recharching;
                    main.ManageButtonState((int)mainState);
                }
                else
                {
                    mainState = States.Active;
                    main.ManageButtonState((int)mainState);
                }
            }

            //handle mortar(burst and volley)
            if (mortarState == States.Recharching)
            {
                mortarCurrentAmmo += 1f / mortarRechargeTime * Time.deltaTime; // recharging
                burst.SetGreenFilling(mortarCurrentAmmo);
                volley.SetGreenFilling(mortarCurrentAmmo);
                if (mortarCurrentAmmo >= 1.0f)
                {
                    mortarState = States.Inactive;
                    burst.ManageButtonState((int)mortarState);
                    volley.ManageButtonState((int)mortarState);
                }
            }
            else if (mortarShouldStop)
            {
                mortarState = States.Inactive;
                burst.ManageButtonState((int)mortarState);
                volley.ManageButtonState((int)mortarState);
                mortarShouldStop = false;
            }
            else if (mortarState == States.Pressed)
            {
                if (isBursting)
                {
                    //make mortar burst  Fire();
                    isBursting = false;
                }
                else
                {
                    //make mortar valley Fire();
                }
                mortarCurrentAmmo = 0f;
                mortarState = States.Recharching;
                burst.ManageButtonState((int)mortarState);
                volley.ManageButtonState((int)mortarState);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void SendPlayerPosition()
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteInteger((int)ClientPackets.СBuyMechan);

        string encryptionKey = Security.security.KeyPath(Security.security.serverKeyPath);

        string encData1 = playerPosition.GetPlayerPosition();

        buffer.WriteEncryptedString(encryptionKey, encData1);

        ClientTCP.clientTCP.SendData(buffer.ToArray());
        buffer.Dispose();
    }
}