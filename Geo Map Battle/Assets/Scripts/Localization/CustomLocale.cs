﻿using UnityEngine;
using Localization;
using UnityEditor;

public static class CustomLocale
{
    [MenuItem("Tools/Russian")]
    private static void RussianLanguage()
    {
        Localize.SetCurrentLanguage(SystemLanguage.Russian);
    }

    [MenuItem("Tools/English")]
    private static void EnglishLanguage()
    {
        Localize.SetCurrentLanguage(SystemLanguage.English);
    }
}