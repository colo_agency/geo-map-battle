﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngarController : MonoBehaviour
{
    private const string OPEN_STOCK = "OpenStock";
    private const string CLOSE_STOCK = "CloseStock";
    Animator animator;
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OpenStock()
    {
        animator.Play(OPEN_STOCK);
    }

    public void CloseStock()
    {
        animator.Play(CLOSE_STOCK);
    }

    public void BuyMechan()
    {
        ClientTCP.clientTCP.GetMechanShop().BuyMechan(1);
    }

    public void OnFightClick()
    {
        NetworkManager.Instance.RegisterForBattle();
    }

}
