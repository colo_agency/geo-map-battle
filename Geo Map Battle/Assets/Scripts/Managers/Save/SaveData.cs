﻿
[System.Serializable]
public class SaveData
{
    string login = "";
    string password = "";

    public string Login
    {
        get { return login; }
        private set { login = value; }
    }

    public string Password
    {
        get { return password; }
        private set { password = value; }
    }

    public SaveData(UserInfo user)
    {
        login = user.Login;
        password = user.Password;
    }
}
