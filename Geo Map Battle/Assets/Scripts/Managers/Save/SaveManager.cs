﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public static class SaveManager {

    static string path = Application.persistentDataPath + "/player.dt";

    public static void SaveData(SaveData user)
    {
        BinaryFormatter formater = new BinaryFormatter();
        FileStream file = new FileStream(path, FileMode.Create);

        formater.Serialize(file, user);
        file.Close();
    }

    public static SaveData LoadData()
    {
        if(File.Exists(path))
        {
            BinaryFormatter formater = new BinaryFormatter();
            FileStream file = new FileStream(path, FileMode.Open);
            SaveData user = formater.Deserialize(file) as SaveData;
            file.Close();
            return user;
        }
        else
        {
            return null;
        }
    }
    public static void DeleteData()
    {
        if(File.Exists(path))
        {
            File.Delete(path);
        }
    }
}
