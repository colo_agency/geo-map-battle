using Localization;
using PopUp;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace MenuManagers
{
    public class MainMenuManager : MonoBehaviour
    {
        [Header("UserInfo")]
        [SerializeField]
        private UserInfo user;

        [Header("Menu Items")]
        [SerializeField]
        private Localize localizeLoginButton;

        [SerializeField]
        private Button loginButton = null;

        [SerializeField]
        private Localize registrationText;

        [SerializeField]
        private PopUpShow popup;

        [SerializeField]
        private Animator registrationPageAnimator;

        [SerializeField]
        private GameObject loginLoader;

        [SerializeField]
        private int testMode = 1;

        [SerializeField]
        private string playerCity = "Kiev"; // [labu] test

        [SerializeField]
        private Localize errorLogin;

        private bool registration = false;

        private string login = "";
        private string password = "";
        private string email = "";

        private int spellcheck = 0;

#if UNITY_EDITOR

        private void OnValidate()
        {
            loginButton = GameObject.Find("LoginButton").GetComponent<Button>();
            localizeLoginButton = loginButton.GetComponentInChildren<Localize>();
            registrationText = GameObject.Find("Registration").GetComponentInChildren<Localize>();
            registrationPageAnimator = transform.Find("Login").GetComponent<Animator>();
            popup = transform.Find("PopUp").GetComponent<PopUpShow>();
        }

#endif

        private string[] loginMsg = {
            "error-connect",
            "error-password",
            "error-in-game",
            "error-login",
            "error-gps"
        };

        //TODO: Add to localization registrationMsg[0] if the sentence will be using
        private string[] registrationMsg = {
            "Successfully registered",
            "error-reg-email",
            "error-reg-fields",
            "error-reg-login",
        };

        public void OnLoginButtonClick()
        {
            if (!registration)
            {
                user.Login = login;
                user.Password = password;
                if (NetworkManager.Instance.Login(login, password, playerCity, testMode))
                {
                    SaveData data = new SaveData(user);
                    SaveManager.SaveData(data);
                    StartCoroutine(WaitAndLogin());
                }
                else
                {
                    errorLogin.ChangeKey(loginMsg[0]); // couldn't connect to server
                    popup.gameObject.SetActive(true);
                    popup.AnimationShow();
                }
            }
            else
            {
                //TODO: Add data validation to the fields before sending a request to the server
                NetworkManager.Instance.Registration(login, password, email);
                //QUESTIONABLE: What is this for? If we are not sure of the correctness of the data in the fields.
                user.Login = login;
                user.Password = password;
                user.Email = email;

                StartCoroutine(WaitAndRegister());
            }
        }

        private IEnumerator WaitAndLogin()
        {
            while (HandleNetworkData.handleNetworkData.Message == -1)
            {
                yield return new WaitForSeconds(0.1f);
            }
            switch (HandleNetworkData.handleNetworkData.Message)
            {
                case 0:
                    WaitAndLoadAngar();
                    break;

                default:
                    errorLogin.ChangeKey(loginMsg[HandleNetworkData.handleNetworkData.Message]);
                    popup.gameObject.SetActive(true);
                    popup.AnimationShow();
                    break;
            }

            HandleNetworkData.handleNetworkData.Message = -1;
        }

        private void WaitAndLoadAngar()
        {
            StartCoroutine(WaitAndLoad());
        }

        private IEnumerator WaitAndLoad()
        {
            loginLoader.SetActive(true);
            yield return new WaitForSeconds(1.2f);
            SceneManager.LoadScene("MainScene");
        }

        private IEnumerator WaitAndRegister()
        {
            //NOTE: Get rid of magic numbers
            while (HandleNetworkData.handleNetworkData.Message == -1)
            {
                yield return new WaitForSeconds(0.1f);
            }

            Debug.Log("Message from server: " + registrationMsg[HandleNetworkData.handleNetworkData.Message]);

            //NOTE: Get rid of magic numbers
            switch (HandleNetworkData.handleNetworkData.Message)
            {
                case 0:
                    WaitAndLoadAngar();
                    break;

                default:
                    errorLogin.ChangeKey(registrationMsg[HandleNetworkData.handleNetworkData.Message]);
                    popup.gameObject.SetActive(true);
                    popup.AnimationShow();
                    break;
            }

            //NOTE: Get rid of magic numbers
            HandleNetworkData.handleNetworkData.Message = -1;
        }

        public void OnRegistrationTextClick()
        {
            if (!registration)
            {
                Input.backButtonLeavesApp = false;
                registrationPageAnimator.SetBool("isRegistration", true);
                localizeLoginButton.localizationKey = "registration";
                registrationText.localizationKey = "login-enter";
            }
            else
            {
                Input.backButtonLeavesApp = true;
                registrationPageAnimator.SetBool("isRegistration", false);
                localizeLoginButton.localizationKey = "enter";
                registrationText.localizationKey = "new-registration";
            }
            registration = !registration;
            CheckSpelling();
            localizeLoginButton.UpdateLocale();
            registrationText.UpdateLocale();
        }

        /// back from registration page
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Input.backButtonLeavesApp = true;
                registrationPageAnimator.SetBool("isRegistration", false);
            }
        }

        /// get/set
        public void SetLogin(string login)
        {
            string loginCheckExpression = "([a-zA-Z])([a-zA-Z0-9]{3,9})";
            if (System.Text.RegularExpressions.Regex.IsMatch(login, loginCheckExpression, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
            {
                Debug.Log($"  (match for '{loginCheckExpression}' found)");
                SpellingCorrect(true);
            }
            else
            {
                Debug.Log("Login not matching");
                SpellingCorrect(false);
            }
            this.login = login;
        }

        private void SpellingCorrect(bool correct)
        {
            if (correct)
            {
                spellcheck++;
            }
            else
            {
                spellcheck--;
            }
            CheckSpelling();
        }

        private void CheckSpelling()
        {
            if (spellcheck >= 2 && !registration)
            {
                loginButton.interactable = true;
            }
            else if (spellcheck >= 3 && registration)
            {
                loginButton.interactable = true;
            }
            else
            {
                loginButton.interactable = false;
            }
        }

        public void SetPassword(string pass)
        {
            string passwordCheckExpression = "([a-zA-Z])([a-zA-Z0-9]{5,9})";
            if (System.Text.RegularExpressions.Regex.IsMatch(pass, passwordCheckExpression, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
            {
                Debug.Log($"  (match for '{passwordCheckExpression}' found)");
                SpellingCorrect(true);
            }
            else
            {
                Debug.Log("password not matching");
                SpellingCorrect(false);
            }
            password = pass;
        }

        public void SetEmail(string email)
        {
            string emailCheckExpression = "([a-zA-Z0-9]{1,20})[@]([a-zA-Z0-9]{1,10})[.]([a-zA-Z0-9]{2,10})";
            if (System.Text.RegularExpressions.Regex.IsMatch(email, emailCheckExpression, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
            {
                Debug.Log($"  (match for '{emailCheckExpression}' found)");
                SpellingCorrect(true);
            }
            else
            {
                Debug.Log("email not matching");
                SpellingCorrect(false);
            }
            this.email = email;
        }
    }
}