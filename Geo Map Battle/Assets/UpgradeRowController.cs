﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeRowController : MonoBehaviour
{
    [SerializeField] GameObject leftButton;
    [SerializeField] GameObject rightButton;
    [SerializeField] UpgradeManager upgradeManager;
    [SerializeField] Text valueText;

    private int value = 80;
    private int initialValue;
    
    public int price = 600;
    public int Value
    {
        set
        {
            this.value = value;
        }
    }

    public void RightButtonClick()
    {
        if(value < 110)
        {
            value += 10;
            ChangeDisplayedValue(1);
        }
    }
    public void LeftButtonClicked()
    {
        if(value > initialValue)
        {
            value -= 10;
            ChangeDisplayedValue(-1);
        }
    }
    private void ChangeDisplayedValue(int sign) // 1 to plus, -1 to subtraction
    {
        valueText.text = value.ToString();
        upgradeManager.ChangeSum( price * sign);
        CheckButtonsVisability();
    }
    private void CheckButtonsVisability()
    {
        if(value <= 80)
        {
            leftButton.SetActive(false);
        } else
        {
            leftButton.SetActive(true);
        }
        if(value >= 110)
        {
            rightButton.SetActive(false);
        }else
        {
            rightButton.SetActive(true);
        }
    }
    void Start()
    {
        initialValue = value;
        CheckButtonsVisability();
    }

}
