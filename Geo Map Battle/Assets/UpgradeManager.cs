﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour
{
    [SerializeField] private Animation animation;
    [SerializeField] private Text sumText;
    private bool isShowing = false;

    private int sumToBuy = 0;
    private void Start()
    {
        ChangeSum(0);
    }
    public void OnImproveButoton()
    {
        if (isShowing)
        {
            animation.Play("ScrollToRight");
            isShowing = false;
        } else
        {
            animation.Play("ScrollFromRight");
            isShowing = true;
        }
    }

    public void OnBuyButton()
    {
        //TODO
    }

    public void ChangeSum(int amount)
    {
        sumToBuy += amount;
        sumText.text = sumToBuy.ToString();
    }
}
