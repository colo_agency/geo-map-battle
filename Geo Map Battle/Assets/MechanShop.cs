﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechanShop : MonoBehaviour
{
    [SerializeField] ClientTCP clientTCP;
    public void BuyMechan(int typeId)
    {
        PacketBuffer buffer = new PacketBuffer();
        buffer.WriteInteger((int)ClientPackets.СBuyMechan);

        string encryptionKey = Security.security.KeyPath(Security.security.serverKeyPath);

        string encData1 = typeId.ToString();

        buffer.WriteEncryptedString(encryptionKey, encData1);

        clientTCP.SendData(buffer.ToArray());
        buffer.Dispose();
    }

}
