﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleUIController : MonoBehaviour
{
    
    float startTime;
    bool isAntiAirFiring;
    
    IEnumerator timeCoroutine = null;
    [SerializeField]
    float antiAirCurrentState = 0f;
    [SerializeField]
    TextMeshProUGUI playerName;
    [SerializeField]
    TextMeshProUGUI enemyName;
    [SerializeField]
    TextMeshProUGUI playerHealth;
    [SerializeField]
    TextMeshProUGUI enemyHealth;
    [SerializeField]
    TextMeshProUGUI time;
    [SerializeField]
    Image[] dronsStatusBars;
    
    private const float RECHARGE = 1.0f;
    private const float FIRE = -1.0f;
    private Color active = new Color(0.2705882f, 0.5372549f, 0.4039216f, 1f);
    private Color ready = new Color(0f, 0.1551485f, 1f, 1f);
    private Color downed = new Color(0.8301887f, 0.2112349f, 0.4039216f, 1f);
    private Color absent = new Color(0.3113208f, 0.3069153f, 0.3069153f, 1f);  // Private constants

    public enum dronState { Active, Ready, Downed, Absent  }

    public void Start()
    {


        // Dron's status bars references

        //Test initialization
        SetPlayerName("Pete"); 
        SetEnemyName("Pete123");
        SetPlayerHealth(96.23414f);
        SetEnemyHealth(56.551f);

        startTime = Time.time;
        timeCoroutine = TimeUpdater();
        StartCoroutine(timeCoroutine);
        
    }

    public void SetPlayerName(string name)
    {
        playerName.text = name;
    }

    public void SetEnemyName(string name)
    {
        enemyName.text = name;
    }

    public void SetPlayerHealth(float health)
    {
        playerHealth.text = Mathf.RoundToInt(health).ToString() + " %";
    }

    public void SetEnemyHealth(float health)
    {
        enemyHealth.text = Mathf.RoundToInt(health).ToString() + " %";
    }

    public void buttonPressed(int button)
    {
        if(button < 4)
        {
            BattleController.Instance.FireButtonPressed(button);
        } else if(button == 4)
        {
            BattleController.Instance.StopMain();
        } else if(button == 5)
        {
            BattleController.Instance.StopAntiAir();
        }
        
    }


    public void StopGame()
    {
        StopCoroutine(timeCoroutine);
    }

    IEnumerator TimeUpdater()
    {
        while (true)
        {
            CalculateTime();
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void CalculateTime()
    {
        float timePassed = Time.time - startTime;
        int minutes = (int)timePassed / 60;
        int seconds = (int)timePassed % 60;
        string separator;
        if (seconds < 10)
        {
            separator = ":0";
        } else
        {
            separator = ":";
        }
        time.text = minutes.ToString() + separator + seconds.ToString();
    }

    public void DronStateChange(int number, dronState state)
    {
        switch (state)
        {
            case dronState.Active:
                dronsStatusBars[number - 1].color = active;
                break;
            case dronState.Ready:
                dronsStatusBars[number - 1].color = ready;
                break;
            case dronState.Downed:
                dronsStatusBars[number - 1].color = downed;
                break;
            case dronState.Absent:
                dronsStatusBars[number - 1].color = absent;
                break;
        } 
    } //UI dron status bar update
}
