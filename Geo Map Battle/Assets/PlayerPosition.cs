﻿using Mapbox.Unity.Location;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPosition : MonoBehaviour
{
    private ILocationProvider _locationProvider;

    private ILocationProvider LocationProvider
    {
        get
        {
            if (_locationProvider == null)
            {
                _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            }

            return _locationProvider;
        }
    }

    public string GetPlayerPosition()
    {
        return LocationProvider.CurrentLocation.LatitudeLongitude.x.ToString() + "/" +
               LocationProvider.CurrentLocation.LatitudeLongitude.y.ToString();
    }
}