﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechanManager : MonoBehaviour
{
    [SerializeField] GameObject[] mechans;
    [SerializeField] GameObject nextButton;
    [SerializeField] GameObject prevButton;

    private int currentMechan = 0;

    private void Start()
    {
        CheckAvailableButtons();
    }
    public void NextMechan() //scripts on buttons in angar
    {
        if (currentMechan + 1 < mechans.Length)
        {
            mechans[currentMechan].GetComponent<Animation>().Play("ScrollToLeft");
            currentMechan++;
        
            mechans[currentMechan].GetComponent<Animation>().Play("ScrollFromRight");
        }
        CheckAvailableButtons();
    }
    public void PrevMechan()//scripts on buttons in angar
    {
        if (currentMechan > 0)
        {
            mechans[currentMechan].GetComponent<Animation>().Play("ScrollToRight");
        currentMechan--;
        
            mechans[currentMechan].GetComponent<Animation>().Play("ScrollFromLeft");
        }
        CheckAvailableButtons();
    }

    private void CheckAvailableButtons() // should we hide buttons? 
    {
        if(currentMechan == 0)
        {
            prevButton.SetActive(false);
        }
        else
        {
            prevButton.SetActive(true);
        }

        if(currentMechan == mechans.Length - 1){
            nextButton.SetActive(false);
        }
        else
        {
            nextButton.SetActive(true);
        }
    }

    public void ManageBMString(string bm)
    {
        throw new System.NotImplementedException(); // TODO parse string to data
    }
}
